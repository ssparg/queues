<?php

namespace App\Jobs\Users;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Storage;
use File;
use Image;

class UpdateImage implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    public $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $id)
    {
        $this->user = $user;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fileName = $this->id . '.png';
        $filePath = storage_path() . '/uploads/' . $this->id;

        Image::make($filePath)->encode('png')->fit(60, 60, function($c){
            $c->upsize();
        })->save();

        if (Storage::disk('s3')->put("images/users/{$fileName}", fopen($filePath, 'r+'))) {
            File::delete($filePath);
        }

        $this->user->image_filename = $fileName;
        $this->user->save();
    }
}
